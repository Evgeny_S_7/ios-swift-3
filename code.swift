let month_days = [
    31,
    28, //пренебрегаем високосным годом
    31,
    30,
    31,
    30,
    31,
    31,
    30,
    31,
    30,
    31,
]

let month_names = [
    "Январь",
    "Февраль",
    "Март",
    "Апрель",
    "Май",
    "Июнь",
    "Июль",
    "Август",
    "Сентябрь",
    "Октябрь",
    "Ноябрь",
    "Декабрь",
]

//здесь можно было использовать индекс и выводить в формате "индекс + 1: кол-во дней", но специально использован такой цикл для демонстрации
for days in month_days {
    print(days)
}
print()

var months = [(String, Int)]() //массив кортежей
for i in 0..<month_days.count {
    print("\(month_names[i]): \(month_days[i])")
    months.append((month_names[i], month_days[i])) //наполняем массив кортежей, чтобы воспользоваться им в следующем цикле
}
print()

for (name, days) in months {
    print("\(name): \(days)")
}
print()

for i in stride(from: month_days.count-1, through: 0, by: -1) {
    print("\(month_names[i]): \(month_days[i])")
}
print()

let month_number = Int.random(in: 0..<month_days.count)
let day_number = Int.random(in: 1...month_days[month_number])

var days_sum = 0
for i in 0..<month_number {
    days_sum += month_days[i]
}
days_sum += day_number - 1 //вычитаем 1 тк сам день не идёт в счёт (до 1 января от начала года 0 дней)

print("Дата: \(day_number) \(month_names[month_number]) \nВсего дней: \(days_sum)")